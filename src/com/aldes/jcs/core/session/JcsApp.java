package com.aldes.jcs.core.session;


public class JcsApp {
	
	private static JcsApp instance = new JcsApp();
	
	private JcsApp() {
		
	}
	
	public static JcsApp getInstance() {
		return instance;
	}
	
}
