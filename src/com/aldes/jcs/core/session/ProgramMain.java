package com.aldes.jcs.core.session;

import javax.swing.JFrame;

public class ProgramMain {
	
	public static final int SCREEN_WIDTH = 370;
	public static final int SCREEN_HEIGHT = 375;
	
	
	public static void main(final String[] args) {
		JFrame f = new JcsGUI();
		f.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		((JcsGUI) f).updateScreenLocation();
		f.setVisible(true);
	}
	
}
