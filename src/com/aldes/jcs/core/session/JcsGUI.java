package com.aldes.jcs.core.session;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class JcsGUI extends JFrame {
	
	enum eLABEL {
		FILE,
		DIR
	};

	/**
	 * 
	 */
	private static final long serialVersionUID = -82135954L;
	private File selectedFile = null;
	private String fileString = "";
	private String file_path = "";
	private String mkdir_path = "";
	private JLabel file_path_label = new JLabel("File Selected:    none", JLabel.RIGHT);
	private JLabel mkdir_path_label = new JLabel("Output Directory:    none", JLabel.RIGHT);
	
	private JTextArea text_area = new JTextArea(10, 32);
	
	private int screen_width = 365;
	private int screen_height = 375;
	
	
	@SuppressWarnings("deprecation")
	public JcsGUI() {
		
		super("MkDir Session"); // window title

		// start creating the GUI
		JPanel file_panel = new JPanel();
		JPanel output_panel = new JPanel();
		JPanel result_panel = new JPanel();
		//file_panel.setBorder(BorderFactory.createEmptyBorder(6, 16, 16, 16));
		file_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		output_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		result_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		getContentPane().add(file_panel, BorderLayout.NORTH);
		getContentPane().add(output_panel, BorderLayout.CENTER);
		getContentPane().add(result_panel, BorderLayout.SOUTH);
		
		// NORTH
		{
			
			JButton session_file = new JButton("Select Session File");
			session_file.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent ev) {
						chooseFile();
					}
				});
			
			JScrollPane scrollPane = new JScrollPane(text_area);
			file_panel.add(scrollPane);
			
			// add to GUI
			output_panel.add(session_file);
			output_panel.add(file_path_label);
			
		}
		
		// CENTER
		{
			JButton output_dir = new JButton("Select Output Directory");
			output_dir.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent ev) {
						chooseDirectory();
					}
				});
			
			
			output_panel.add(output_dir);
			output_panel.add(mkdir_path_label);
		}
		
		// SOUTH
		{
			JButton mkdir = new JButton("Make Directory");
			mkdir.addActionListener(
				new ActionListener() {
					public void actionPerformed(ActionEvent ev) {
						makeDirectory();
					}
				});
			
			result_panel.add(mkdir);
		}

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		setResizable(false);
		updateScreenLocation();
	}
	
	public void updateScreenLocation() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();
		int xPos = (dim.width / 2) - (this.getWidth() / 2);
		int yPos = (dim.height / 2) - (this.getHeight() / 2);

		this.setLocation(xPos, yPos);		
	}
	
	public void updateLabel(final eLABEL label) {
		
		if (label == eLABEL.FILE)
		{
			if (file_path.equals(""))
				return;
			
			file_path_label.setText("File Selected: " + file_path);
		}
		else if (label == eLABEL.DIR)
		{
			if (mkdir_path.equals(""))
				return;
			mkdir_path_label.setText("Output Directory: " +  mkdir_path);
		}

	}

	public void chooseFile() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System
				.getProperty("user.home")));
		fileChooser.setDialogTitle("Select the session file...");

		int result = fileChooser.showOpenDialog(this);

		if (result != JFileChooser.APPROVE_OPTION) {
			// user selects a file
			return; 
		}

		// get the file
		selectedFile = fileChooser.getSelectedFile();
		
		// get the file path
		file_path = fileChooser.getSelectedFile().toString();
		file_path = file_path.replace("\\", "/");
		System.out.println("\nFile Path: " + file_path + "\n");
		
		updateLabel(eLABEL.FILE);
	}

	public void chooseDirectory() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System
				.getProperty("user.home")));
		fileChooser.setDialogTitle("Select the output directory...");
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//
		// disable the "All files" option.
		//
		fileChooser.setAcceptAllFileFilterUsed(false);
		//
		if (fileChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
			System.out.println("No Selection ");
			return;
		}
		
		System.out.println("getCurrentDirectory(): " + fileChooser.getCurrentDirectory());
		System.out.println("getSelectedFile() : " + fileChooser.getSelectedFile());
		
		mkdir_path = fileChooser.getSelectedFile().toString();
		mkdir_path = mkdir_path.replace("\\", "/");
		mkdir_path += "/";
		System.out.println("\nDirectory Path: " + mkdir_path + "\n");
		
		updateLabel(eLABEL.DIR);
	}
	
	public void makeDirectory() {
		
		fileString = text_area.getText();
		System.out.println("\nFile String: \n" + fileString + "\n\n");
		
		if (selectedFile == null && fileString.equals(""))
		{
			JOptionPane.showMessageDialog(null, "Please Select the file or Input the Directory name to the Text Area...");
			return;
		}
		
		if (mkdir_path.equals("")) {
			JOptionPane.showMessageDialog(null, "Please Select the output directory...");
			return;
		}
		
		
		if (selectedFile != null) {
			try (
					BufferedReader br = new BufferedReader(new FileReader(selectedFile))
					) {
				
				String directoryName_inLine;
				while ((directoryName_inLine = br.readLine()) != null) {
					System.out.println(directoryName_inLine);
					
					File theDir = new File(mkdir_path + directoryName_inLine);
					
					if(!theDir.exists()) 
						theDir.mkdir();
				}
				
			} catch(Exception e) {
				System.err.println(e);
			}
		}
		
		if (!fileString.equals("")) {
			
			for (String directoryName_inLine : text_area.getText().split("\\n")) {
				System.out.println(directoryName_inLine);
				
				File theDir = new File(mkdir_path + directoryName_inLine);
				
				if(!theDir.exists()) 
					theDir.mkdir();
			}
		}
		
	}

}
