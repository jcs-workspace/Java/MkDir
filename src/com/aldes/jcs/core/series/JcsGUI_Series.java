package com.aldes.jcs.core.series;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;

public class JcsGUI_Series extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 95218434L;
	private int screen_width = 600;
	private int screen_height = 400;

	private String file_name = "";
	private String output_path = "";
	private JLabel output_label = new JLabel("Output Directory:    none", JLabel.RIGHT);
	private JTextField fileName_textField = new JTextField(20);
	private JSpinner start_spinner = null;
	private JSpinner end_spinner = null;
	
	private static int MAX_DIRECTORY_CREATE = 1000;
	private static int MIN_DIRECTORY_CREATE = 0;

	public JcsGUI_Series() {
		super("MkDir_Series");

		JPanel file_panel = new JPanel();
		JPanel output_panel = new JPanel();
		JPanel result_panel = new JPanel();

		file_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		//output_panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		//result_panel.setLayout(new FlowLayout(FlowLayout.LEFT));

		getContentPane().add(file_panel, BorderLayout.NORTH);
		getContentPane().add(output_panel, BorderLayout.CENTER);
		getContentPane().add(result_panel, BorderLayout.SOUTH);

		// NORTH
		{
			// Text Field
			fileName_textField.setToolTipText("Directory Name");
			file_panel.add(fileName_textField);
			
			// Start Spinner
			JLabel label_1 = new JLabel("Start Index: ");
			SpinnerModel model_1 = new SpinnerNumberModel(0, MIN_DIRECTORY_CREATE, MAX_DIRECTORY_CREATE, 1);
			start_spinner = new JSpinner(model_1);
			file_panel.add(label_1);
			file_panel.add(start_spinner);
			
			// Last Spinner
			JLabel label_2 = new JLabel("Last Index: ");
			SpinnerModel model_2 = new SpinnerNumberModel(0, MIN_DIRECTORY_CREATE, MAX_DIRECTORY_CREATE, 1);
			end_spinner = new JSpinner(model_2);
			file_panel.add(label_2);
			file_panel.add(end_spinner);
			
		}

		// CENTER
		{
			// Button
			JButton output_dir = new JButton("Select Output Directory");
			output_dir.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent ev) {
					chooseDirectory();
				}
			});

			output_panel.add(output_dir);
			output_panel.add(output_label);		// Label
		}
		
		// SOUTH
		{
			JButton mkdir = new JButton("Make Directory");
			mkdir.addActionListener(
					
				new ActionListener() {
					
					public void actionPerformed(ActionEvent ev) {
						
						file_name = fileName_textField.getText();
						makeDirectory(output_path, 
								file_name, 
								(Integer)start_spinner.getValue(), 
								(Integer)end_spinner.getValue());
					}
				});
			
			result_panel.add(mkdir);
		}
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		pack();
		setVisible(true);
		setResizable(false);
		updateScreenLocation();
	}
	
	public void updateScreenLocation() {
		Toolkit tk = Toolkit.getDefaultToolkit();
		Dimension dim = tk.getScreenSize();
		int xPos = (dim.width / 2) - (this.getWidth() / 2);
		int yPos = (dim.height / 2) - (this.getHeight() / 2);

		this.setLocation(xPos, yPos);		
	}

	public void chooseDirectory() {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(System
				.getProperty("user.home")));
		fileChooser.setDialogTitle("Select the output directory...");
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		//
		// disable the "All files" option.
		//
		fileChooser.setAcceptAllFileFilterUsed(false);
		//
		if (fileChooser.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
			System.out.println("No Selection ");
			return;
		}

		System.out.println("getCurrentDirectory(): "
				+ fileChooser.getCurrentDirectory());
		System.out.println("getSelectedFile() : "
				+ fileChooser.getSelectedFile());

		output_path = fileChooser.getSelectedFile().toString();
		output_path = output_path.replace("\\", "/");
		output_path += "/";
		System.out.println("\nDirectory Path: " + output_path + "\n");

		updateLabel();
	}

	public void updateLabel() {

		if (output_path.equals(""))
			return;
		output_label.setText("Output Directory: " + output_path);

	}

	public void makeDirectory(final String location, final String fileName, int indexOne, int indexTwo) {
		
		if (file_name.equals("")) {
			JOptionPane.showMessageDialog(null, "Please enter the base file name!");
			return;
		}
		
		if (indexOne >= indexTwo) {
			JOptionPane.showMessageDialog(null, "Start index(" + indexOne + ") cannot be higher than or equal to Last index(" + indexTwo + ") !!!");
			return;
		}
		
		if (output_path.equals("")) {
			JOptionPane.showMessageDialog(null, "Please Select the output directory...");
			return;
		}
		
		for (int i = indexOne; i <= indexTwo; ++i) {

			String directoryName = "";

			if (i < 10)
				directoryName = fileName + "_00" + i;
			else if (i < 100)
				directoryName = fileName + "_0" + i;
			else if (i < 1000)
				directoryName = fileName + "_" + i;

			File theDir = new File(location + directoryName);

			try {
				// if the directory does not exist, create it
				if (!theDir.exists())
					theDir.mkdir();
				else
					System.out.println("Directory are already exists: "
							+ directoryName);
			} catch (SecurityException se) {
				return;
			}
			System.out.println("DIR created");
		}
	}

}
