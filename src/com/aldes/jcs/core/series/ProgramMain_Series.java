package com.aldes.jcs.core.series;

import javax.swing.JFrame;

public class ProgramMain_Series {
	
	
	private final static int SCREEN_WIDTH = 550;
	private final static int SCREEN_HEIGHT = 175;
	
	public static void main(final String[] args) {
		JcsGUI_Series f = new JcsGUI_Series();
		// set size out after "new operator" will override the size
		f.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
		f.updateScreenLocation();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setVisible(true);
	}
	
}
